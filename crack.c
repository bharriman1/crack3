#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

int file_length(char *filename)
{ 
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1)
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char line[PASS_LEN];
    guess = md5(line, strlen(line));

    // Compare the two hashes
    if (strcmp(hash, guess) == 0)
    {
        free(guess);
        return 1;
    }
    else
    {
        free(guess);
        return 0;
    }
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    *size = 0;
    int len = file_length(filename);
    char *dict = malloc(len);
    
    //Read in the dictionary file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s for reading", filename);
    }
    
    fread(dict, sizeof(char), len, f);
    fclose(f);
    
    int num =0;
    
    for(int i=0; i<len; i++)
    {
        if (dict[i] == '\n')
        {
            dict[i] = '\0';
            num++;
        }
    }
    
    char **dictionary = malloc(num * sizeof(char *));

    dictionary[0] = dict;
    int j=1;
    for (int i=0; i<len-1; i++)
    {
        if (dict[i] == '\0')
        {
            dictionary[j] = &dict[i+1];
            j++;
        }
    }
    
    size = &j;
    free(dict);
    free(dictionary);
    
    return dictionary;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading", argv[1]);
        exit(2);
    }

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    char line[HASH_LEN];
    while(fgets(line, HASH_LEN, fp) != NULL)
    {
        for (int j=0; j<dlen; j++)
        {
            if(tryguess(line, dict[j]) == 1)
            {
                printf("%s %s", line, dict[j]);
            }
        }
    }
}
